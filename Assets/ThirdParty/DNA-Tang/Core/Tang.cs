using System;
using System.Collections.Generic;
using UnityEngine;

public class Tang : MonoBehaviour
{
    public static event EventHandler Disconnected;

    public static event EventHandler Connected;

    public static event EventHandler<ErrorEventArgs> Error;

    public static event EventHandler<CommandReceivedEventArgs> CommandReceived;

    protected ITangProtocol _protocol;

    protected static Tang _instance;

    protected IList<Action> _queue;

    // Temporary hard coded
    public static Project Project;

    protected static Tang Instance {
        get {
            if (_instance == null) {
                return null;
            } else {
                return _instance;
            }
        }
    }

    ~Tang() {
        if (_protocol != null) {
            _protocol.Connected -= OnConnectedToTANGHandler;
            _protocol.Disconnected -= OnDisconnectedHandler;
            _protocol.Error -= OnErrorHandler;
            _protocol.CommandReceived += OnCommandReceivedHandler;
            _protocol.Disconnect();
        }
    }

    public static void Initialize(ITangProtocol protocol) {
        GameObject go = new GameObject("Tang");
        _instance = go.AddComponent<Tang>();
        _instance._Initialize(protocol);
    }

    public static void Connect(string hostname, int port) {
        Instance?._Connect(hostname, port);
    }

    public static void Disconnect() {
        Instance?._protocol.Disconnect();
    }

    public static void SendCommand(Command command) {
        ClientMessage message = new ClientMessage { Command = command };
        Instance?._protocol.Write(message);
    }

    protected void _Initialize(ITangProtocol protocol) {
        _protocol = protocol;
        _queue = new List<Action>();

        // temp
        Project = new Project("REVES",
            new Moment[3] {
                new Moment("moment-1", "Nord",
                    new Layer[4] {
                        new Layer("Tang"),
                        new Layer("Encens"),
                        new Layer("Dragon"),
                        new Layer("Etoiles")},
                    new Sound[2] {
                        new Sound("murmure", "Murmure"),
                        new Sound("souffle", "Souffle")}),
                new Moment("moment-2", "Ouest",
                    new Layer[1] {
                            new Layer("Dragon") },
                    new Sound[0]),
                new Moment("moment-3", "Sud", new Layer[0], new Sound[0])
            },
            new Music[3]{
                new Music("music-1", "M�lodie 1"),
                new Music("music-2", "M�lodie 2"),
                new Music("music-3", "M�lodie 3")});
    }

    protected void _Connect(string hostname, int port) {
        _protocol.Connect(hostname, port);
        _protocol.Error += OnErrorHandler;
        _protocol.Connected += OnConnectedToTANGHandler;
        _protocol.Disconnected += OnDisconnectedHandler;
    }

    protected void Update() {
        lock (_queue) {
            while (_queue.Count > 0) {
                Action Callback = _queue[0];
                Callback.Invoke();
                _queue.Remove(Callback);
            }
        }
    }

    protected void RunOnMainThread(Action action) {
        lock (_queue) {
            _queue.Add(action);
        }
    }

    protected void OnConnectedToTANGHandler(object sender, EventArgs e) {
        _protocol.Connected -= OnConnectedToTANGHandler;
        _protocol.CommandReceived += OnCommandReceivedHandler;
        RunOnMainThread(() => Connected?.Invoke(this, EventArgs.Empty));
    }

    protected void OnCommandReceivedHandler(object sender, CommandReceivedEventArgs e) {
        RunOnMainThread(() => CommandReceived?.Invoke(this, e));
    }

    protected void OnErrorHandler(object sender, ErrorEventArgs e) {
        RunOnMainThread(() => Error?.Invoke(this, e));
    }

    protected void OnDisconnectedHandler(object sender, EventArgs e) {
        RunOnMainThread(() => Disconnected?.Invoke(this, e));
    }
}