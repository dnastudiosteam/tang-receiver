public class Sound
{
    public string Id { get; }

    public string Name { get; }

    public Sound(string id, string name) {
        Id = id;
        Name = name;
    }
}