public class Music
{
    public string Id { get; }

    public string Name { get; }

    public Music(string id, string name) {
        Id = id;
        Name = name;
    }
}