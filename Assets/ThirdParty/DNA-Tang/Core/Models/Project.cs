using System;
using System.Linq;

public class Project
{
    public event EventHandler MomentChanged;

    private Moment _currentMoment;

    public string Name { get; }

    public Moment[] Moments { get; }

    public Music[] Musics { get; }

    public Moment CurrentMoment => _currentMoment;

    public Project(string name, Moment[] moments, Music[] musics) {
        Name = name;
        Moments = moments;
        Musics = musics;
        _currentMoment = moments[0];
    }

    public void ChangeMoment(Moment moment) {
        _currentMoment = moment;
        MomentChanged?.Invoke(this, EventArgs.Empty);
    }

    public Moment GetMoment(string id) {
        return Moments.FirstOrDefault(m => m.Id == id);
    }
}