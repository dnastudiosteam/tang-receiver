using System;
using System.Collections.Generic;

public class Layer
{
    public event EventHandler<ValueChangedEventArgs> ValueChanged;

    public string Id { get; }

    public IDictionary<string, string> _data;

    public Layer(string id) {
        Id = id;
        _data = new Dictionary<string, string>();
    }

    public void Update(string key, string value) {
        if (!_data.ContainsKey(key)) {
            _data.Add(key, value);
            ValueChanged?.Invoke(this, new ValueChangedEventArgs(key, value));
        } else {
            string current = _data[key];
            _data[key] = value;
            if (current != value) {
                ValueChanged?.Invoke(this, new ValueChangedEventArgs(key, value));
            }
        }
    }

    public float GetFloat(string key) {
        try {
            return float.Parse(_data[key]);
        } catch (Exception) {
            return 0f;
        }
    }

    public string GetString(string key) {
        try {
            return _data[key];
        } catch (Exception) {
            return "";
        }
    }
}

public class ValueChangedEventArgs : EventArgs
{
    public string Data { get; }
    public string Value { get; }

    public ValueChangedEventArgs(string data, string value) {
        Data = data;
        Value = value;
    }
}