using UnityEngine;

public class Moment
{
    public string Id { get; }

    public string Name { get; }

    public Color Color { get; }

    public Layer[] Layers { get; }

    public Sound[] Sounds { get; }

    public Moment(string id, string name, Layer[] layers, Sound[] sounds) {
        Id = id;
        Name = name;
        Layers = layers;
        Sounds = sounds;
    }
}