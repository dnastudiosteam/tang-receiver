using System.Diagnostics;
using UnityEditor;
using UnityEngine;

public class MenuUtils : MonoBehaviour
{
    [MenuItem("REVES/Update proto files")]
    static void UpdateProto() {
        Process.Start(Application.dataPath + @"\ThirdParty\DNA-Tang\Core\update-proto.bat");
    }
}
