using UnityEngine;

public class TangBootstraper : MonoBehaviour
{
    [SerializeField]
    private string _tangAdress = "138.68.69.62";

    [SerializeField]
    private int _tangPort = 3232;

    void Awake() {
        Tang.Initialize(new TangTcp());
        Tang.Connect(_tangAdress, _tangPort);
    }

    private void OnDestroy() {
        Tang.Disconnect();
    }
}