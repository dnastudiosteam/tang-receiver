using Google.Protobuf;
using System;
using System.Net.Sockets;
using System.Threading;
using Debug = UnityEngine.Debug;

public class TangTcp : ITangProtocol
{
    public event EventHandler Connected;

    public event EventHandler Disconnected;

    public event EventHandler<ErrorEventArgs> Error;

    public event EventHandler<CommandReceivedEventArgs> CommandReceived;

    protected TcpClient _socketConnection;

    protected Thread _listenerThread;

    protected string _hostname;

    protected int _port;

    ~TangTcp() {
        Disconnect();
    }

    public void Connect(string hostname, int port) {
        try {
            _listenerThread = new Thread(new ThreadStart(() => Listen(hostname, port)));
            _listenerThread.IsBackground = true;
            _listenerThread.Start();
        } catch (Exception exception) {
            Error?.Invoke(this, new ErrorEventArgs($"Could not connect to TANG: {exception}"));
        }
    }

    public void Disconnect() {
        _listenerThread?.Abort();
        _socketConnection?.Close();
        Disconnected?.Invoke(this, EventArgs.Empty);
    }

    /// <summary> 	
    /// Runs in background clientReceiveThread; Listens for incomming data. 	
    /// </summary>     
    protected virtual void Listen(string hostname, int port) {
        try {
            _socketConnection = new TcpClient(hostname, port);
            byte[] bytes = new byte[1024];
            Connected?.Invoke(this, EventArgs.Empty);

            while (true) {
                using (NetworkStream stream = _socketConnection.GetStream()) {
                    ServerMessage message;
                    while ((message = ServerMessage.Parser.ParseDelimitedFrom(stream)) != null) {
                        HandleMessage(message);
                    }
                }
            }
        } catch (SocketException exception) {
            Error?.Invoke(this, new ErrorEventArgs($"Could not listen from TANG: {exception}"));
        }
    }

    private void HandleMessage(ServerMessage message) {
        if (message.Command != null) {
            CommandReceived?.Invoke(this, new CommandReceivedEventArgs(message.Command));
        }
    }

    public void Write(ClientMessage message) {
        if (_socketConnection == null) {
            Debug.LogWarning("Cannot write to stream: no connection has been established.");
            return;
        }

        try {
            NetworkStream stream = _socketConnection.GetStream();
            if (stream.CanWrite) {
                message.WriteDelimitedTo(stream);
            }
        } catch (Exception exception) {
            Error?.Invoke(this, new ErrorEventArgs($"Could not write to stream: {exception.Message}"));
        }
    }
}