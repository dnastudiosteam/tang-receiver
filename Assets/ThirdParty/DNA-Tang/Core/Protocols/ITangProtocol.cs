using System;

public interface ITangProtocol
{
    public event EventHandler Connected;

    public event EventHandler Disconnected;

    public event EventHandler<ErrorEventArgs> Error;

    public event EventHandler<CommandReceivedEventArgs> CommandReceived;

    public void Connect(string hostname, int port);

    public void Disconnect();

    public void Write(ClientMessage message);
}

public class CommandReceivedEventArgs : EventArgs
{
    public Command Command { get; }

    public CommandReceivedEventArgs(Command command) {
        Command = command;
    }
}

public class ErrorEventArgs : EventArgs
{
    public string Message { get; }

    public ErrorEventArgs(string message) {
        Message = message;
    }
}