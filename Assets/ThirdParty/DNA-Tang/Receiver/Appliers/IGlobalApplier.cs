﻿public interface IGlobalApplier
{
    public void Apply(string value);
}