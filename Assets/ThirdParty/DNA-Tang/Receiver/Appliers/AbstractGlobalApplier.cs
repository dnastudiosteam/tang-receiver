using UnityEngine;

public abstract class AbstractGlobalApplier : MonoBehaviour, IGlobalApplier
{
    public abstract void Apply(string value);
}