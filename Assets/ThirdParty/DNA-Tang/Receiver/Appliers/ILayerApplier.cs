﻿public interface ILayerApplier
{
    public void SetLayer(Layer layer);
}