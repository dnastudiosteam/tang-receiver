using UnityEngine;

public class RedshiftApplier : AbstractLayerApplier
{
    [SerializeField]
    private Transform _redshift;

    [SerializeField]
    private float _smoothnessFactor = 6f;

    private const float _redshiftMin = .5f;

    private const float _redshiftMax = 3f;

    protected override void SetDefault() {
        Vector3 redshift = _redshift.localPosition;
        redshift.x = _redshiftMin + GetFloat() * (_redshiftMax - _redshiftMin);
        _redshift.localPosition = redshift;
    }

    protected override void Update() {
        Vector3 redshift = _redshift.localPosition;
        redshift.x = Mathf.Lerp(redshift.x, _redshiftMin + GetFloat() * (_redshiftMax - _redshiftMin), Time.deltaTime * _smoothnessFactor);
        _redshift.localPosition = redshift;
    }
}