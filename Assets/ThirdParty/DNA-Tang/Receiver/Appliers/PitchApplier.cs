using UnityEngine;

public class PitchApplier : AbstractLayerApplier
{
    [SerializeField]
    private Transform _rotation;

    [SerializeField]
    private float _smoothnessFactor = 6f;

    protected override void SetDefault() {
        Vector3 euler = _rotation.eulerAngles;
        euler.y = GetFloat() * 360f;
        _rotation.eulerAngles = euler;
    }

    protected override void Update() {
        Vector3 euler = _rotation.eulerAngles;
        euler.z = Mathf.LerpAngle(euler.z, GetFloat() * 360f, Time.deltaTime * _smoothnessFactor);
        _rotation.eulerAngles = euler;
    }
}