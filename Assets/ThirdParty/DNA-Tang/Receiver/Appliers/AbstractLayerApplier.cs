using UnityEngine;

public abstract class AbstractLayerApplier : MonoBehaviour, ILayerApplier
{
    [SerializeField]
    protected string _transformation;

    [SerializeField]
    protected string _defaultValue;

    protected Layer _layer;

    public virtual void SetLayer(Layer layer) {
        _layer = layer;
        _layer.ValueChanged += OnValueChangedHandler;
        if (!string.IsNullOrEmpty(_defaultValue)) {
            // TODO: default values should come out of layers
            layer.Update(_transformation, _defaultValue);
            SetDefault();
        }
    }

    private void OnDestroy() {
        if (_layer != null) {
            _layer.ValueChanged -= OnValueChangedHandler;
        }
    }

    protected virtual void SetDefault() { }

    protected virtual void Update() { }

    protected virtual void OnValueChanged() { }

    protected float GetFloat() {
        return _layer.GetFloat(_transformation);
    }

    protected string GetString() {
        return _layer.GetString(_transformation);
    }

    private void OnValueChangedHandler(object sender, ValueChangedEventArgs e) {
        if (e.Data == _transformation) {
            OnValueChanged();
        }
    }
}