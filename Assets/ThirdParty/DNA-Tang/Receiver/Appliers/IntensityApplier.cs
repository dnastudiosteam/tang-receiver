using UnityEngine;

public class IntensityApplier : AbstractLayerApplier
{
    [SerializeField]
    private Renderer _renderer;

    protected override void Update() {
        SetAlpha(_layer.GetFloat(_transformation));
    }

    private void SetAlpha(float a) {
        Color c = _renderer.material.color;
        c.a = a;
        _renderer.material.color = c;
    }
}