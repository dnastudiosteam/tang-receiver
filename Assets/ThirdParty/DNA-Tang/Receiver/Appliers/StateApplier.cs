using UnityEngine;

public class StateApplier : AbstractLayerApplier
{
    [SerializeField]
    private GameObject _gameObject;

    protected override void SetDefault() {
        _gameObject.SetActive(GetString() == "on");
    }

    protected override void OnValueChanged() {
        base.OnValueChanged();
        string state = _layer.GetString(_transformation);
        _gameObject.SetActive(GetString() == "on");
        if (_gameObject.activeSelf) {
            GetComponent<LayerElement>().OnClicked();
        }
    }
}