using UnityEngine;

public class TangReceiver : MonoBehaviour
{
    [SerializeField]
    private LayerCommandInterpreter _layerInterpreter;

    [SerializeField]
    private MomentCommandInterpreter _momentInterpreter;

    [SerializeField]
    private GlobalCommandInterpreter _globalInterpreter;

    private void Start() {
        Tang.CommandReceived += OnTangCommandReceivedHandler;
    }

    private void OnDestroy() {
        Tang.CommandReceived -= OnTangCommandReceivedHandler;
    }

    private void OnTangCommandReceivedHandler(object sender, CommandReceivedEventArgs e) {
        Command command = e.Command;
        switch (command.Scope) {
            case "layer":
                _layerInterpreter.Execute(command);
                break;
            case "moment":
                _momentInterpreter.Execute(command);
                break;
            case "global":
                _globalInterpreter.Execute(command);
                break;
        }
    }
}