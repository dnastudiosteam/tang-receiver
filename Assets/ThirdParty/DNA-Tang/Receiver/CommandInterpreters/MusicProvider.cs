using System.Linq;
using UnityEngine;

public class MusicProvider : MonoBehaviour
{
    [SerializeField]
    private MusicElement[] _musics;

    public MusicElement GetMusic(string id) {
        return _musics.FirstOrDefault(m => m.Id == id);
    }
}