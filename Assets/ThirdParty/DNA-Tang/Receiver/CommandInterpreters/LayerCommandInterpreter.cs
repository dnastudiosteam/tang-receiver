using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(LayerProvider))]
public class LayerCommandInterpreter : MonoBehaviour, ICommandInterpreter
{
    public event EventHandler<LayerSelectedEventArgs> LayerSelected;

    [SerializeField]
    private Transform _container;

    private LayerProvider _provider;

    private Layer[] _layers;

    private LayerElement[] _layersController;

    private LayerElement _selectedLayer;

    private void Awake() {
        _provider = GetComponent<LayerProvider>();
    }

    private void Start() {
        _layersController = new LayerElement[0];
        Tang.Project.MomentChanged += OnMomentChangedHandler;
    }

    private void OnDestroy() {
        foreach (LayerElement layer in _layersController) {
            layer.Clicked -= OnLayerSelectedHandler;
        }
        Tang.Project.MomentChanged -= OnMomentChangedHandler;
    }

    public void Execute(Command command) {
        LayerElement layer = _layersController.FirstOrDefault(l => l.Layer.Id == command.LayerId);
        if (layer != null) {
            GetLayerById(command.LayerId)?.Update(command.Transformation, command.Value);
        }
    }

    private void ClearLayers() {
        IList<GameObject> destroy = new List<GameObject>();
        foreach (Transform layer in _container) {
            destroy.Add(layer.gameObject);
        }
        foreach (GameObject go in destroy) {
            DestroyImmediate(go);
        }
        LayerSelected?.Invoke(this, new LayerSelectedEventArgs(null));
    }

    private void CreateLayers(Layer[] layers) {
        IList<LayerElement> controllers = new List<LayerElement>();
        foreach (Layer layer in layers) {
            LayerElement layerCtrl = Instantiate(_provider.GetLayer(layer.Id), _container);
            layerCtrl.Clicked += OnLayerSelectedHandler;
            layerCtrl.SetLayer(layer);
            controllers.Add(layerCtrl);
        }
        _layersController = controllers.ToArray();
    }

    private Layer GetLayerById(string layerId) {
        return _layers.FirstOrDefault(l => l.Id == layerId);
    }

    private void OnLayerSelectedHandler(object sender, EventArgs e) {
        if (_selectedLayer != null) {
            _selectedLayer.Deselect();
        }
        _selectedLayer = (LayerElement)sender;
        _selectedLayer.Select();
        LayerSelected?.Invoke(this, new LayerSelectedEventArgs(_selectedLayer.Layer));
    }

    private void OnMomentChangedHandler(object sender, EventArgs e) {
        _layers = Tang.Project.CurrentMoment.Layers;
        ClearLayers();
        CreateLayers(_layers);
    }
}

public class LayerSelectedEventArgs : EventArgs
{
    public Layer Layer { get; }

    public LayerSelectedEventArgs(Layer layer) {
        Layer = layer;
    }
}