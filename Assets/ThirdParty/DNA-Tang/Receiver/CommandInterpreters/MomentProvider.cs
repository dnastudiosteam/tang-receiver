using System.Linq;
using UnityEngine;

public class MomentProvider : MonoBehaviour
{
    [SerializeField]
    private MomentElement[] _moments;

    public MomentElement GetMoment(string id) {
        return _moments.FirstOrDefault(l => l.Id == id);
    }
}