public interface ICommandInterpreter
{
    public void Execute(Command command);
}
