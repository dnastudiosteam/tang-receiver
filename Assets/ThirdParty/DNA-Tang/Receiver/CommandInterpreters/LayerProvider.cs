using System.Linq;
using UnityEngine;

public class LayerProvider : MonoBehaviour
{
    [SerializeField]
    private LayerElement[] _layers;

    public LayerElement GetLayer(string id) {
        return _layers.FirstOrDefault(l => l.Id == id);
    }
}