using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(MomentProvider))]
public class MomentCommandInterpreter : MonoBehaviour, ICommandInterpreter
{
    [SerializeField]
    private Transform _container;

    private MomentProvider _momentsProvider;

    private SoundElement[] _sounds;

    private void Awake() {
        _momentsProvider = GetComponent<MomentProvider>();
    }

    public void Execute(Command command) {
        switch (command.Transformation) {
            case "moment":
                Moment moment = Tang.Project.GetMoment(command.Value);
                if (moment != null) {
                    SetMoment(moment);
                }
                break;
            case "sound":
                PlaySound(command.Value);
                break;
        }
    }

    private void Clear() {
        IList<GameObject> destroy = new List<GameObject>();
        foreach (Transform layer in _container) {
            destroy.Add(layer.gameObject);
        }
        foreach (GameObject go in destroy) {
            DestroyImmediate(go);
        }
    }

    private void SetMoment(Moment moment) {
        Clear();
        MomentElement prefab = _momentsProvider.GetMoment(moment.Id);
        if (prefab != null) {
            MomentElement ctrl = Instantiate(prefab);
            ctrl.transform.SetParent(_container);
            ctrl.transform.localPosition = Vector3.zero;
        }
        _sounds = GetComponentsInChildren<SoundElement>();
        Tang.Project.ChangeMoment(moment);
    }

    private void PlaySound(string value) {
        _sounds.FirstOrDefault(s => s.SoundId == value)?.Play();
    }
}