using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(MusicProvider))]
public class GlobalCommandInterpreter : MonoBehaviour, ICommandInterpreter
{
    [SerializeField]
    private Transform _container;

    [SerializeField]
    private bool _playSoundsAndRecorder = true;

    private MusicProvider _musicProvider;

    private IList<MusicElement> _musics;

    // temporary flag to disable sounds e.g. in the controller
    private MusicElement _playing;

    private void Awake() {
        _musicProvider = GetComponent<MusicProvider>();
    }

    private void Start() {
        _musics = new List<MusicElement>();
        foreach (Music music in Tang.Project.Musics) {
            MusicElement prefab = _musicProvider.GetMusic(music.Id);
            if (prefab != null) {
                MusicElement ctrl = Instantiate(prefab);
                ctrl.transform.SetParent(_container);
                ctrl.transform.localPosition = Vector3.zero;
                _musics.Add(ctrl);
            }
        }
    }

    public void Execute(Command command) {
        if (!_playSoundsAndRecorder) {
            return;
        }
        switch (command.Transformation) {
            case "music":
                PlayMusic(command.Value);
                break;
            case "recording":
                Record(command.Value);
                break;
        }
    }

    private void PlayMusic(string value) {
        MusicElement ctrl = _musics.FirstOrDefault(m => m.Id == value);
        _playing?.Stop();
        ctrl?.Play();
        _playing = ctrl;
    }

    private void Record(string value) {
        switch (value) {
            case "record":
                Recorder.Recorder.Record();
                break;
            case "stop":
                Recorder.Recorder.Stop();
                break;
            case "play":
                Recorder.Recorder.Play();
                break;
        }
    }
}