using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundElement : MonoBehaviour
{
    [SerializeField]
    private string _soundId;

    private AudioSource _source;

    public string SoundId => _soundId;

    private void Awake() {
        _source = GetComponent<AudioSource>();
    }

    public void Play() {
        AudioSource clone = Instantiate(_source);
        clone.Play();
        clone.transform.position = _source.transform.position;
        Destroy(clone.GetComponent<SoundElement>());
        Destroy(clone.gameObject, clone.clip.length);
    }
}
