using System;
using UnityEngine;

public class LayerElement : MonoBehaviour
{
    public event EventHandler Clicked;

    [SerializeField]
    private string _id;

    public string Id => _id;

    public Layer Layer => _layer;

    [SerializeField]
    private Color _unselectedColor;

    [SerializeField]
    private Color _selectedColor;

    [SerializeField]
    private Renderer _renderer;

    [SerializeField]
    private MouseDownTransmitter _mouseDown;

    private ILayerApplier[] _appliers;

    private Layer _layer;

    private void Awake() {
        _appliers = GetComponents<AbstractLayerApplier>();
        _mouseDown.MouseDown += OnMouseDownHandler;
    }

    private void OnMouseDownHandler(object sender, EventArgs e) {
        OnClicked();
    }

    public void SetLayer(Layer layer) {
        gameObject.name = layer.Id;
        _layer = layer;
        foreach (var applier in _appliers) {
            applier.SetLayer(layer);
        }
    }

    public void OnDestroy() {
        _mouseDown.MouseDown -= OnMouseDownHandler;
    }

    public void Deselect() {
        CopyColor(_unselectedColor);
    }

    public void Select() {
        CopyColor(_selectedColor);
    }

    public void OnClicked() {
        Clicked?.Invoke(this, EventArgs.Empty);
    }

    private void CopyColor(Color color) {
        Color c = _renderer.material.color;
        c.r = color.r;
        c.g = color.g;
        c.b = color.b;
        _renderer.material.color = c;
    }
}
