using UnityEngine;

public class MomentElement : MonoBehaviour
{
    [SerializeField]
    private string _id;

    public string Id => _id;
}
