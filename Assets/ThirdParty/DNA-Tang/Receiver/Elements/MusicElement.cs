using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class MusicElement : MonoBehaviour
{
    [SerializeField]
    private string _id;

    private AudioSource _source;

    private float _tVolume = 0f;

    private float _bVolume = 0f;

    public string Id => _id;

    private void Awake() {
        _source = GetComponent<AudioSource>();
        _bVolume = _source.volume;
        _source.volume = 0f;
    }

    private void Update() {
        _source.volume = Mathf.Lerp(_source.volume, _tVolume, Time.deltaTime * 2f);
    }

    public void Play() {
        _source.Stop();
        _source.Play();
        _tVolume = _bVolume;
    }

    public void Stop() {
        _tVolume = 0f;
    }
}
