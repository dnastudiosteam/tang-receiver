using System;
using UnityEngine;

public class MouseDownTransmitter : MonoBehaviour
{
    public event EventHandler MouseDown;

    private void OnMouseDown() {
        MouseDown?.Invoke(this, EventArgs.Empty);
    }
}
